# Agradeço a DEUS pelo dom do conhecimento
# Eduardo Marçal
# Criado em 27.05.15

# Exemplos de Input and Output
.text

.globl inicio

inicio:
jal ler_inteiro_do_teclado
la $t7, 0($v0)
jal imprime_inteiro
j fim

ler_inteiro_do_teclado:
li $v0, 5
syscall
jr $ra

imprime_inteiro:
li $v0, 1
la $a0, ($t7)
syscall
jr $ra

fim:
li $v0, 10
syscall