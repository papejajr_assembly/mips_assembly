# Agradeço a DEUS pelo dom do conhecimento
# Eduardo Marçal
# Criado em 26.05.15

# Exemplos de Leitura e escrita via console

# Imprime um valor inteiro Estaticamente
add $a0, $zero, 24	# adiciona no $a0 (argumento)o valor 24
li $v0, 1			# Codigo de print do sistema no registro $v0
syscall

# ler um valor inteiro, armazenado na memória RAM
li $v0, 5			# Ler um inteiro
syscall
la $t7, 0($v0)		# Carrega o valor digitado pelo ususario para $t7
li $v0, 1			# Codigo de print do sistema
la $a0, ($t7)		# Passa o valor para o arguemnto para ser impresso
syscall

# Imprimindo uma string
.data
strings: .asciiz "\nOla Mundo. \n"

.text

main: 
li $v0, 4
la $a0, strings
syscall
