# Agradeço a DEUS pelo dom do conhecimento
# Eduardo Marçal
# Criado em 26.05.15

# Exemplos de Estrutura Condicional

.text

main:

# Adicionando os valores aos regitros
add $t1, $zero, 5
add $t2, $zero, 6
add $t3, $zero, 7

# b target		# pula para o label
			# 1 - Verdadeiro	0 - Falso
beq $t0, $t1, target	# verifica se $t0 = $t1
blt $t1, $t0, target	# verifica se $t0 < $t1
ble $t3, $t1, target	# verfica se $t1 <= $t3
bgt $t0, $t2, target	# verifica se $t0 > $t2
bge $t1, $t3, target	# verifica se $t1 >= $t3
bne $t0, $t1, target	# verifica se $t0 <> $t1

target:

j main			# pula para o label main, sem condição
# jr $t3			# pula para o endereço $t3 do registro

