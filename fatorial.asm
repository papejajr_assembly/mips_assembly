# Agradeço a DEUS pelo dom do conhecimento
# Eduardo Marçal
# Crado em 28.05.15

.data
vi: .word 1			# aloca espaço para a variável global
vj: .word 1			# aloca espaço para a variável global

resp: .ascii "Fatorial de 5\n"

.text

addi $t1, $zero, 1	# j = 1

# cria os argumentos do for (i=1; i<=n; i++) 
addi $t4, $zero, 1		# inicialização
slti $t7, $zero, 6			# condição
beq $t7, $zero, fimfor		# atualização

for: 	slti $t7, $t4, 6		# n<6 gera o fat(5)
	mult $t1, $t4		# j = j($t1) * i($t4) 
	mflo $t4
	addi $t4, $t4, 1		# incremento do i ($t4), ou seja, i = i + 1
	j for				# volta novamente para o inicio do for


fimfor:	addi $v0, $zero, 4	# imprimir uma string
		la $a0, resp
		syscall
		
		addi $v0, $zero, 1
		addu 

# return(0)
li $v0, 10
syscall
