# Agradeço a DEUS pelo dom do conhecimento
# Eduardo Marçal
# Criado em 26.05.15

# Exemplos de Declaração de variaveis

.data
var1: .word 23		# cria uma variavel e armazena o valor 3 nela do tipo inteira
array1: .byte 'a','b'	# cria um array de dois elementos
array2: .space 12 	# aloca 40 consecutivos bytes da memória

.text
_start:
lw $t0, var1	# Ler o conteúdo da RAM e inserir no registrador $t0
li $t1, 5	# Realizar um carregamento intermediario atribuindo a $t1 = 5
sw $t1, var1	# armazena o conteúdo do reistrador $t1 na memória RAM: var1 = $t1
