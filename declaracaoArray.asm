# Agradeço a DEUS pelo dom do conhecimento
# Eduardo Marçal
# Criado em 26.05.15

# Exemplos de Declaração de variaveis do tipo array

.data
array2: .space 12 	# aloca 40 consecutivos bytes da memória

.text
_start:
la $t0, array2	# Ler o endereço da memória e inserir no registrador $t0
li $t1, 5	# intermediar entre a memória e o registrador
sw $t1, ($t0)	# Adiciona o primeiro elemento do array para o valor que estar no registro $t0, indiretamente
li $t1, 13	# Atribui o valor 13 ao $t1
sw $t1, 4($t0)	# Adiciona o valor de $t1 no segundo indice do array
li $t1, -7	# Atribui o valor -7 ao registro $t1
sw $t1, 8($t0)	# Adiciona o valor de $t1 no indice de 3 (8($t0))
