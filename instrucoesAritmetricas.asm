# Agradeço a DEUS pelo dom do conhecimento
# Eduardo Marçal
# Criado em 26.05.15

# Exemplos de Instruções Aritmetricas
.text

add $8, $zero, 10

# Só vai fazer sentido, se adicionar os valores no respectivos registros

add $t0, $t1, $t2	# $t0 = $t1 + $t2
sub $t2, $t3, $t4	# $t2 = $t3 - $t4
addi $t2, $t3, 5	# $t2 = $t3 + 5 adicao imediata
addu $t1, $t3, $t7	# Adiciona inteiros não assinados
subu $t1, $t6, $t7	# Subtrai inteiros não assinados

mult $t3, $t4		# Multiplicação 32 bits, caso ultrapasse usar-se o Hi(Example: $t5 mod $t6 Resto) e Lo (Example: $t1 / $t6 Quociente)

mfhi $t0		# Move a quantidade (valor) do registro Hi para $t0, ou seja, $t0 = Hi
mflo $t1		# Move a quantidade (valor) do registro Lo para $t1, ou seja, $t1 = Lo

move $t2, $t3		# Move o valor do registro $t2 para $t3, ou seja, $t2 = $t3

